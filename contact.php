<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact Us | Houston, Texas Branding &amp; Website Services</title>
<meta name="description" content="Contact Middle Brain Media |  Branding and Website services for Houston, Texas area businesses!">
<?php 

include ('assets/includes/header.php');

$thisPage ="Contact"; 

?>



</head>
  <body id="contact" class="top">
  <?php include ('assets/includes/navigation.php'); ?>
  <div class="content">

  <div class="brain-bg">
    <div class="container">
    <div class="row">
            <div class="col-xs-12 col-md-5 text">
                <h2>Drop Us a Line</h2>
                <p>Thank you so much for stopping our website. If you are a seriously passionate entrepreneur ready for a brand overhaul, We would love to hear from you.</p>

                <p>Please fill out this contact form with as much detail as possible so that We may learn more about you, your business, and your vision.</p>

                    <p>If you would like to get in touch with us by phone, please contact us at <a href="tel:281-891-3022">281-891-3022</a></p>

                
            </div>
            <div class="col-xs-12 col-md-7">
                <form action="send_data.php" method="POST">
            <div class="row">
            <div class="col-sm-12 col-md-4 item">
                    <label>Name:</label>
                    <input type="text" class="required form-control" placeholder="Name" name="name" />
                    <span>please tell us your name</span>
            </div>
            <div class="col-sm-12 col-md-4 item">
                    <label>Phone:</label>
                    <input type="text" class="required form-control" placeholder="Phone" name="phone"  />
                    <span>Please tell us your phone number</span>
            </div>
            <div class="col-sm-12 col-md-4 item">
                    <label>Email:</label>
                    <input type="text" class="required form-control" placeholder="Email" name="email"  />
                    <span>Please tell us your email address</span>
            </div>

            </div>
    
            <div class="row">
                <div class="col-sm-12 col-md-6 item">
            <label>Business Name:</label>
                    <input type="text" class="required form-control" placeholder="Business Name" name="business"  />
                    <span>Please tell us the name of your business or organization</span>
            </div>

            <div class="col-sm-12 col-md-6 item">
            <label>Web Address [If you have one]:</label>
                    <input type="text" class="required form-control" placeholder="Web address" name="web_address"  />
                    <span>Please tell us your website address</span>
            </div>
            </div>
            

            <div class="row">
                <div class="col-sm-12">
            <h5>Desired Services</h5>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-3 item">
            <input type="checkbox" class="required form-control" placeholder="Web Design" name="web_design" value="yes"  />
            <label>Web Design:</label>
                <span>Please tell us the name of your business or organization</span>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-3 item">
            <input type="checkbox" class="required form-control" placeholder="Branding" name="branding" value="yes"  />
            <label>Branding:</label>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-3 item">
            <input type="checkbox" class="required form-control" placeholder="Web & Branding" name="web_branding" value="yes"/>
            <label>Web & Branding:</label>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-3 item">
            <input type="checkbox" class="required form-control" placeholder="Other" name="other" value="yes"  />
            <label>Other:</label>
            </div>
            </div>
            

            <div class="row">
                <div class="col-sm-12">
            <label>Describe your Business:</label>
                    <textarea name="describe"  class="form-control" rows="3" ></textarea>
                    <span>Please tell us the name of your business or organization</span>
            </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
            <label>Your Message:</label>
                    <textarea name="comment"  class="form-control" rows="3" ></textarea>
                    <span>Please tell us the name of your business or organization</span>
            </div>
            </div>
            
            <div class="row field1-wrapper">
                <div class="col-sm-12">
                    <input type="text" name="field1" tabindex="-1" autocomplete="off">
                    <div class="g-recaptcha" data-sitekey="6LdXOGYdAAAAAPfaTIdp5CbQz_4Wr2EmJ3hc4ZOe" data-callback="verifyCaptcha"></div>
                    <div id="g-recaptcha-error"><div style="color:red;">This field is required.</div></div>
                </div>

            <div class="col-sm-12">
                    <button type="submit" id="submit" class="btn btn-primary">submit</button>
                </div>
            </div><!-- End -->
          </form>
        </div>

    </div>
    </div>

    <br /><br /><br /><br /><br /><br /><br /><br />


    <!-- <div class="row">
              <div class="container">
                <div class="row solid-red-bg">
                    <div class="col-xs-12 col-sm-4 item">
                    <h3>We know how to design</h3>
                    <img src="./assets/images/contact-graphic.png" at="Twitter" class="img-responsive" />
                    </div>
                    <div class="col-xs-12 col-sm-4 item">
                    <h3>We also know how to build websites</h3>
                    <img src="./assets/images/map.png" at="Twitter" class="img-responsive" />
                    </div>
                    <div class="col-xs-12 col-sm-4 item">
                    <h3>From start to launch</h3>
                    <img src="./assets/images/contact-graphic.png" at="Twitter" class="img-responsive" />
                    </div>
                </div>
                <h2>WE ARE TWO MINDS BRIDGING THE GAP BETWEEN THE CREATIVE AND ANALYTIC NATURE OF DESIGN AND DEVELOPMENT</h2>
            </div>        
        </div> -->



</div>

       



<?php include ('assets/includes/footer.php'); ?>




   

    
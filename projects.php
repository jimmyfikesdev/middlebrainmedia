<!DOCTYPE html>
<html lang="en">
<head>
<title>Kingwood, Texas Branding &amp; Website Development Projects</title>
<meta name="description" content="Check out our work developing websites for Houston, TX businesses!">
<?php 
include ('assets/includes/header.php');
$thisPage ="Services"; 
?>



</head>
  <body id="projects" class="top">
  <?php include ('assets/includes/navigation.php'); ?>
  <div class="content">

  <div class="row no-padding no-margin mustard">
    <div class="col-xs-12">
    <h1>Our Projects</h1>
    <p class="text-center">Where logic, reasoning, creativity, and expression meet to<span>create a perfect balance in the products we deliver to our clients</span></p>
    </div>
  </div>

  <div class="row no-padding no-margin air clients">
    <div class="col-xs-12 col-sm-4">
    	<img src="./assets/images/air-logo.png" alt="Art In Residence" class="img-responsive" />
    	<span class="no_line">exploring the potential of public art in Antelope Valley, CA</span>
    	<a href="https://artinresidence.gallery/" class="btn" target="_blank">View Project</a>
    </div>
  </div>

  <div class="row no-padding no-margin artmemo clients">
    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
      <img src="./assets/images/artmemo-logo.png" alt="Freedom Chassrs" class="img-responsive" />
      <span>A Peek Into The Art World</span>
      <a href="http://artmemomagazine.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>
  

  <div class="row no-padding no-margin highlevel clients">
    <div class="col-xs-12 col-sm-4">
      <img src="./assets/images/highlevel.png" alt="Freedom Chassrs" class="img-responsive" />
      <span>high level sports talk</span>
      <a href="http://highleveltalk.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>

  <div class="row no-padding no-margin banafrit clients">
    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
        <img src="./assets/images/banafrit-logo.png" alt="Banafrit Photography" class="img-responsive" />
        <span>black pinups in los angeles</span>
        <a href="http://banafrit.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>

  <div class="row no-padding no-margin blackgun clients">
    <div class="col-xs-12 col-sm-4">
    	<img src="./assets/images/blackgun.png" alt="Alina Zayas" class="img-responsive" />
    	<span>a comprehesive list of black gun shops</span>
    	<a href="http://blackgunresourceguide.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>


  <div class="row no-padding no-margin onpoint clients">
    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
    	<img src="./assets/images/onpoint-logo.png" alt="Alina Zayas" class="img-responsive" />
    	<span>south florida videography & photography</span>
    	<a href="http://onpoint-video-photo.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>

  <div class="row no-padding no-margin alina clients">
    <div class="col-xs-12 col-sm-4">
    	<img src="./assets/images/alina-logo.png" alt="Alina Zayas" class="img-responsive" />
    	<span>music is the language of the universe</span>
    	<a href="http://www.alinazayas.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>

  <div class="row no-padding no-margin seasoned clients">
    <div class="col-xs-12 col-sm-4 col-sm-offset-8">
      <img src="./assets/images/seasoned-logo.png" alt="Seasoned Reviews" class="img-responsive" />
      <span>We’re doing reviews a bit different.</span>
      <a href="http://seasonedreviews.com/" class="btn" target="_blank">View Project</a>
    </div>
  </div>


<?php include ('assets/includes/footer.php'); ?>




   

    
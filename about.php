<!DOCTYPE html>
<html lang="en">
<head>
<title>Get to Know Middle Brain Media in Houston, Texas</title>
<meta name="description" content="Meet Middle Brain Media, a Houston, Texas branding and web design team combining creativity and logic.">
<?php 
include ('assets/includes/header.php');
$thisPage ="About"; 
?>



</head>
  <body id="about" class="top">
  <?php include ('assets/includes/navigation.php'); ?>
  <div class="content">
  <div class="about-bg">

      <div class="row">
	      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
	      <h1 class="text-center">what is<span>MIDDLE BRAIN MEDIA?</span></h1>
<img src="./assets/images/2minds.png" at="Alina Zayas Projects" class="img-responsive img-center" />
<p class="text-center max-600">We are a professional team focused on identifying and implementing effective branding strategies for our clients. We want to create final products that are meaningful, effective and a pleasure for all to see.</p>
	      </div>
	  </div>

	  <div class="row ffc21f">
	  <div class="col-xs-12 margbtm20">
	  <h3>HOW WE CAME TO BE</h3>
	  </div>
				<div class="col-xs-12 col-sm-6">
					
					<p>Middle Brain Media is a two-person team, a designer and developer, with a passion for creating branding, websites, and print materials our clients—and their audiences—will love. We call ourselves Middle Brain Media because we bring together an explosively creative mind and admirably logical one to create final products that are just the right balance of form and function. It’s like the ideal marriage, rare but amazing and rewarding, and our clients reap the benefits.</p>
				</div>
				<div class="col-xs-12 col-sm-6">
					<p>We have years of experience honing the skills that make us professionals at what we do. Our clients are generally small to mid-sized businesses and artists in the Houston, Texas area, including restaurants, salons, and musicians, seeking to further their business or creative goals by establishing a unique and impactful presence in person and online. If you’re planning to launch a brand, business, or website, we hope you’ll consider our services to get yours started.</p>
				</div> 
	  </div> 

	  <div class="row">
	  		<div class="col-xs-12 col-sm-6 col-sm-offset-3">
	  		<h3 class="text-center">Let’s Get Started on Your Project!</h3><a href="contact.php" class="btn">Request a Quote</a>
	  		</div>
	  </div>  
	  


</div><!-- about -bg -->
<?php include ('assets/includes/footer.php'); ?>




   

    
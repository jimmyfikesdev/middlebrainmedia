<!DOCTYPE html>
<html lang="en">
<head>
<title>Houston, Texas Branding &amp; Website Services</title>
<meta name="description" content="Check out our branding and website services for Houston, Texas area businesses!">
<?php 
include ('assets/includes/header.php');
$thisPage ="Services"; 
?>



</head>
  <body id="services" class="top">
  <?php include ('assets/includes/navigation.php'); ?>
  <div class="content">


<h1 class="text-center margtop30">Branding &amp; Website Services for Houston, Texas Businesses</h1>
<div class="yellowbox">
  <div class="row">
  
    <div class="col-lg-8 col-lg-offset-2 ">
      <p>Thank you for checking out our branding and website services for Houston, Texas area businesses! Each brand design or website project takes approximately 5 to 8 weeks. Full brand/print/web projects typically take anywhere from 3+ months. All time estimates are dependent upon the individual project, number and complexity of revisions, client response time, and our current booking schedule. A detailed schedule with estimated milestones is provided to each client upon project start.</p>
    </div>
  </div>
 </div>

  <div class="row">
	  <div class="col-lg-10 col-lg-offset-1 white-bg">
	    <h3>Branding</h3>
	    <p>Our branding package is a fun, personal brand development process customized to your business or creative goals. We make it a priority to provide you with quality work and the attention you deserve, so you feel like part of the team and involved in conceptualizing and building your brand. We encourage ongoing communication, from our initial interview to client questionnaires to phone calls and emails. Through contact, we find out exactly what you’re looking for and make your ideas come to life! More than designer, we aim to be the brand consultant and sounding board you need, and to create a final product that reflects your style and serves as an effective marketing tool for years to come.</p> 
	    </div>
	  </div>

		  <div class="red-bg">
			  <div class="row">
				  <div class="col-lg-12">
				  <h2>Our Branding Process</h2>
				  </div>
				  <div class="col-lg-6">
							<h5>Consult & Questionnaire</h5>
							<p>Like any relationship, we’ll meet first to make sure we are a good match. We’ll ask you some questions regarding your business, vision, and goals for your brand. You’ll also get the chance to ask us any questions that come to mind. This gives us both a chance to get to know one another and make sure we’re going to make an awesome team.</p>

							<h5>Booking</h5>
							<p>If we hit it off, we’ll put together a formal project proposal and sent it your way. If you approve our proposal, please, sign, scan, and send the proposal back to us and keep a copy for yourself. You will also kindly pay the invoice* for our first milestone to hold your spot in our queue at this time. *Please note this deposit is nonrefundable.</i></p>

							<h5>Inspire</h5>
							<p>The first step in creating your brand will be pulling together an inspiration board. Using your questionnaire, we’ll find inspiration to define a color palette and style that will set the overall tone of your new brand. You’ll receive your first deliverable within 10 business days of your start date and have a chance to review and request up to 2 rounds of revisions until you’re in love with the direction.</p>
				    </div>
				    <div class="col-lg-6">
				    		<h5>Create</h5>
							<p>Once your inspiration board is finalized we’ll get to work on creating your logo. The logo design process can take 10 to 15 business days to complete. During this phase you‘ll receive your second deliverable which will consist of a few logo concepts designed based on the information given in the initial stages and your approved inspiration board. Once received, you’ll have the opportunity to review and request up to 2 rounds of revisions until your are completely happy with your new logo design.</p>

							<h5>Brand Delivery</h5>
							<p>Now we pull everything together to create your final deliverable: your new brand style guide! We’ll include your new logo, color palette, and inspiration board. Then we’ll add your sub mark, logo variations, social icons, patterns, fonts and any other elements into a full branding guide format. You’ll have a chance to review and request up to 2 rounds of revisions on the guide. Once we receive your approval, and you’ve made your final payment, your files will be packaged and sent to you via a convenient Google Drive link for download.</p>

							<h5>Print & Social Branding</h5>
							<p>We now take your newly designed brand and continue it through print materials, such as a business card, business letterhead, notecard, and envelope. We’ll also provide a social media page design (Facebook and Twitter banner designs, etc.) to complete your seamless, professional new look. Your files are delivered in print-ready PDF form, or you can choose to have us manage your printing needs.</p>
				    </div>
			    </div>
		  </div>

  <div class="row"><div class="col-lg-10 col-lg-offset-1 white-bg">
    <h3>Web Design & Development</h3>
    <p>We build beautiful, effective websites completely customized to your brand and business needs. We will design and professionally develop your new website from the ground up to represent your new or existing brand online, and make sure it’s responsive so it looks perfect on any screen size. No website is worth building if it can’t easily be found. We’ll optimize your website for search engines like Google, so your audience can find your site quickly and without difficulty. If you need the ability to manage your website yourself, we can build your website on WordPress, and even give you a tutorial to get you on your way.</p> 
    </div>
  </div>


		  <div class="red-bg2">
			  <div class="row">
				  <div class="col-lg-12">
				  <h2>Our Website Design &amp; Development Process</h2>
				  </div>
				  <div class="col-lg-6">
							<h5>Design</h5>
							<p>After you complete your website questionnaire and provide all of your content for each page of your new website, we start designing. All site photography and content is due prior to website mockup completion and before we go to the development stage (all files shared by Google Drive folder). During the design phase, the final mockup of the website home page and all interior pages are completed and approved by you before moving on to the development phase. Any delays in providing content may delay your project and bump it back in our queue. If you request content changes or add content after mockup approval, these changes and additions will be billed separately.</p>
				    </div>
				    <div class="col-lg-6">
				    		<h5>Development</h5>
							<p>Once you have signed off on the website design, your site goes to our expert website developer. You’ll have some downtime during this phase as we work to bring your design to life. Please allow approximately 2-4 weeks for us to develop and fine-tune your website. At the end of this step, you will receive a link to a live test site hosted on our server for your review and approval.</p>

							<h5>Launch</h5>
							<p>Once your website is complete, and you’ve reviewed and approved it, we will upload it to your server. If you’ve purchased a WordPress website, we will also hold our tutorial at this time to show you how to update your website from the backend. Once our final checklist is complete, we’ll launch your new website for all to see!</p>
				    </div>
			    </div>
		  </div>




<?php include ('assets/includes/footer.php'); ?>




   

    
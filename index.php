
<!DOCTYPE html>
<html lang="en">
<head>
<title>Kingwood, Texas Custom Wordpress Development</title>
<meta name="description" content="We’re Middle Brain Media, a creative print and digital design and development team based in Kingwood, Texas specializing in custom Wordpress design and development.">
<?php include ('assets/includes/header.php');
$thisPage ="Home"; 
?>

</head>
  <body id="home" class="top">
  <?php include ('assets/includes/navigation.php'); ?>
  <div class="content">
        

        <div class="row home-main">
          <div class="col-lg-8 col-lg-offset-2">
            <img src="./assets/images/home_main.png" at="Twitter" class="img-responsive" />
          </div>
        </div>

      <div class="brown">
          <div class="row">
              <div class="col-lg-8 col-lg-offset-2">
              <h1 class="text-center">Compelling Print &amp; Digital Design for Kingwood, Texas Businesses<span></span></h1>
              <p>Greetings! We’re Middle Brain Media, a print and digital design and development team

based in Kingwood, Texas. Our goal is to bring together imagination and logic to create

compelling brand identities and websites for small businesses, entrepreneurs, and

artists. Our work is visually exciting and it makes sense.</p>
              </div>
          </div>
          

          <div class="row home-sub">
            <div class="col-lg-10 col-lg-offset-1">
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <img src="./assets/images/fingerprint_sub.png" at="Twitter" class="img-responsive" />
                  <h4>Branding</h4>
                  <p>We will work with you to identify and conceptualize your style, and develop an effective, lasting marketing tool for your business.</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <img src="./assets/images/cpu_sub.png" at="Twitter" class="img-responsive" />
                  <h4>Web Design</h4>
                  <p>We design beautiful, impactful websites, seamlessly incorporating your new or existing brand into your online home.</p>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <img src="./assets/images/code_sub.png" at="Twitter" class="img-responsive" />
                  <h4>Web Development</h4>
                  <p>We build fully optimized, responsive websites that look perfect on every device and are easily found online.</p>
                </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <a href="services.php" class="btn img-center">OUR SERVICES</a>
                  </div>
              </div>
            </div>
            </div>
      </div><!-- Brown -->

      

      <div class="row white-sub">
      <div class="col-lg-10 col-lg-offset-1">
      <h3 class="text-center">Let’s Get Started on Your Project!</h3>
                    <p>Please contact Middle Brain Media in Kingwood, Texas to learn more about our services and to schedule a consultation with our team. We think creating your brand and your website should be a fun and rewarding experience, not a chore. Let us help you find your identity and further your business. We work with clients throughout the Kingwood, Texas area.</p>
         <p>We know how to design. We also know how to build websites. While these two skills need to go together to create a meaningful digital identity, it doesn’t always work that way—to the detriment of many small businesses and individuals who need a web presence. That’s where we come in. We approach websites and branding differently than most design companies, who prioritize looks over functionality, and most development companies, who don’t care enough about visual appeal. Our constant objective and overarching purpose is to do everything right, add the twist you didn’t see coming, and arrive at the ideal result.</p>
         <a href="projects.php" class="btn">See Our Work</a>
      </div>
      </div>
      



<?php include ('assets/includes/footer.php'); ?>




   

    
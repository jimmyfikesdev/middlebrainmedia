<div class="row mic-bg">
  <div class="col-xs-12 col-sm-12 col-md-6 no-padding margtop50">
    <h3>music is the<span class="block-tablet-up">language of the universe</span></h3>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 no-padding">
    <h4>Upcoming Shows</h4>

    <div class="row margtop40">
      <div class="col-xs-12 col-sm-6">
        <h5>The Industry Bar - May 29</h5>
        <p>&bull; Karaoke Sundays: 9PM - 1AM</p>
      </div>
      <div class="col-xs-12 col-sm-6">
        <h5>The Industry Bar - Jun 10</h5>
        <p>&bull; Karaoke Thursdays: 9PM - 1AM</p>
      </div>
      <div class="col-xs-12 col-sm-6">
        <h5>RAGE - May 30</h5>
        <p>&bull; Musical Monday: 10PM</p>
      </div>
      <div class="col-xs-12 col-sm-6">
        <h5>The Industry Bar - June 22</h5>
        <p>&bull; Spotlight Artists: 9PM - 1AM</p>
      </div>
      <div class="col-xs-12 col-sm-6">
        <h5>Lucky Strike at LA Live - June 2</h5>
        <p>&bull; Spotlight Artist: 7:30PM</p>
      </div>
    </div>
  </div>
</div>
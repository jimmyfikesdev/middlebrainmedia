<?php include ('assets/includes/functions.php'); ?>
              
<div class="main logo menu-bg menu" id="top">
 <div class="row">
    <div class="col-sm-3">
      <a href="./" alt="" /><img src="./assets/images/logo.png" alt="Retha J. Jewelry" class="img-responsive" /></a>
    </div>
    <div class="col-sm-9 links hidden-xs">
      <nav class="navbar navbar-inverse">
      <ul class="nav navbar-nav">
        <li><a href="projects.php" alt="" class="projects" />Projects</a>
                            <!-- <ul>
                              <li><a href="projects-flookys.php">New Flooky's</a></li>
                              <li><a href="projects-retha.php">Retha J. Jewelry</a></li>
                              <li><a href="projects-alina.php">Alina Zayas</a></li>
                            </ul> -->
                            </li>
                            <li><a href="services.php" alt="" class="services" />Services</a></li>
                            <li><a href="about.php" alt="" class="about" />About</a></li>
                            <li><a href="contact.php" alt="" class="contact" />Contact</a></li>
                            <li class="contact_btn"><a href="tel:281-891-3022">Call Now</a></li>
      </ul>
    </nav>
    </div>
  </div>
  </div>

  <!-- Mobile Nav -->
  <div class="row links sml main hidden-lg hidden-md hidden-sm">
                <nav class="navbar navbar-default navbar-fixed-top navbar-inversed">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Mobile Nav -->
                    <div class="collapse navbar-static" id="collapse" style="text-indent:20px;">
                       <ul class="nav navbar-nav">
                            <li><a href="projects.php" alt="" class="<?php if ($thisPage=="projects") echo " here"; ?>" />Projects</a>
                            <!-- <ul>
                              <li><a href="projects-flookys.php">New Flooky's</a></li>
                              <li><a href="projects-retha.php">Retha J. Jewelry</a></li>
                              <li><a href="projects-alina.php">Alina Zayas</a></li>
                            </ul> --></li>
                            <li><a href="services.php" alt="" class="<?php if ($thisPage=="services") echo " here"; ?>" />Services</a></li>
                            <li><a href="about.php" alt="" class="<?php if ($thisPage=="about") echo " here"; ?>" />About</a></li>
                            <li><a href="contact.php" alt="" class="<?php if ($thisPage=="contact") echo " here"; ?>" />Contact</a></li>
                            <li><a href="tel:281-891-3022">Call Now</a></li>
                        </ul>
                    </div>
                </nav>
              </div>

